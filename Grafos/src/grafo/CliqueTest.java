package grafo;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class CliqueTest 
{
	@Test (expected=IllegalArgumentException.class)
	public void cliqueConjuntoNullTest() 
	{
		Grafo g = new Grafo(5);
		g.esClique(null);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void cliqueConjuntoNegativoTest()
	{
		Grafo g = new Grafo(5);
		Set<Integer> conjunto = new HashSet<Integer>();
		conjunto.add(-1);
		g.esClique(conjunto);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void cliqueConjuntoExcedidoTest()
	{
		Grafo g = new Grafo(5);
		Set<Integer> conjunto = new HashSet<Integer>();
		conjunto.add(5);
		g.esClique(conjunto);
	}
	
	@Test
	public void cliqueVacioTest()
	{
		Grafo g = new Grafo(5);
		Set<Integer> conjunto = new HashSet<Integer>();
		assertTrue(g.esClique(conjunto));
	}
	
	@Test
	public void cliqueUnitarieTest()
	{
		Grafo g = new Grafo(5);
		Set<Integer> conjunto = new HashSet<Integer>();
		conjunto.add(1);
		assertTrue(g.esClique(conjunto));
	}
	
	@Test
	public void cliqueVariosTest()
	{
		Grafo g = new Grafo(5);
		g.agregarArista(1, 3);
		
		Set<Integer> conjunto = new HashSet<Integer>();
		conjunto.add(1);
		conjunto.add(3);

		assertTrue(g.esClique(conjunto));
	}
	
	@Test
	public void cliqueVariosFalseTest()
	{
		Grafo g = new Grafo(5);
		g.agregarArista(1, 3);
		
		Set<Integer> conjunto = new HashSet<Integer>();
		conjunto.add(0);
		conjunto.add(1);

		assertFalse(g.esClique(conjunto));
	}

	@Test
	public void cliqueVecinosDeTodoTest()
	{
		Grafo g = ejemplo();
		
		Set<Integer> conjunto = new HashSet<Integer>();
		conjunto.add(0);
		conjunto.add(1);
		conjunto.add(2);
		conjunto.add(3);
		
		assertTrue(g.esClique(conjunto));
	}

	@Test
	public void cliqueVecinosDeTodoFalseTest()
	{
		Grafo g = ejemplo();

		Set<Integer> conjunto = new HashSet<Integer>();
		conjunto.add(0);
		conjunto.add(1);
		conjunto.add(3);
		conjunto.add(4);
		
		assertFalse(g.esClique(conjunto));
	}

	private Grafo ejemplo() 
	{
		Grafo g = new Grafo(5);
		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(1, 2);
		g.agregarArista(1, 3);
		g.agregarArista(2, 3);
		g.agregarArista(3, 0);
		g.agregarArista(2, 4);
		return g;
	}
}
