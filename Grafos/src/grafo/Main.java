package grafo;

public class Main {

	public static void main(String[] args)
	{
		Grafo g = new Grafo(5);

		g.agregarArista(0, 1);
		g.agregarArista(1, 2);
		g.agregarArista(1, 3);
		g.agregarArista(0, 3);
		g.agregarArista(3, 4);
		
		System.out.print("Existe arista 1,0 ");
		System.out.println(g.existeArista(1, 0));
		System.out.print("Existe arista 0,1 ");
		System.out.println(g.existeArista(0, 1));

	}

}
