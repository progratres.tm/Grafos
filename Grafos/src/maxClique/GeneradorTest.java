package maxClique;

public class GeneradorTest implements Generador 
{
	private int numero;
	private boolean[] _bits;
	private int actual = 0;
	
	public GeneradorTest(boolean[] bits, int num) 
	{
		numero = num;
		_bits = bits;
		
	}

	@Override
	public boolean nextBoolean() 
	{
		return _bits[actual++];
	}

	@Override
	public int nextInt(int number) 
	{
		return numero;
	}

}
