package maxClique;

import java.util.HashSet;
import java.util.Set;
import grafo.Grafo;

public class Solver 
{
	private Grafo _grafo ;
	private Set<Integer> _incumbent;
	private static long _tiempoInicial;
	private static long _tiempoFinal;
	private int _hojasGeneradas;


	public Solver(Grafo g)
	{
		_grafo = g;
	}

	public Set<Integer> resolver()
	{
		_hojasGeneradas = 0;
		_incumbent = new HashSet<Integer>();
		_tiempoInicial = System.currentTimeMillis();

		Set<Integer> vacio = new HashSet<Integer>();
		recursion(0, vacio);

		_tiempoFinal = System.currentTimeMillis();
		return _incumbent;
	}

	private void recursion(int actual, Set<Integer> conjunto)
	{
		if( actual == _grafo.vertices() )
		{
			// Caso base
			if( _grafo.esClique(conjunto) && conjunto.size() > _incumbent.size() )
				_incumbent = new HashSet<Integer>(conjunto);

			_hojasGeneradas++;
		}
		else
		{
			// Caso recursivo
			conjunto.add(actual);
			recursion(actual+1, conjunto);

			conjunto.remove(actual);
			recursion(actual+1, conjunto);
		}
	}

	// Estadísticas
	public int hojas()
	{
		return _hojasGeneradas;
	}

	public static double darTiempo()
	{
		return ((_tiempoFinal-_tiempoInicial)/1000.0);
	}
}


