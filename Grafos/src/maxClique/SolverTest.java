package maxClique;

import static org.junit.Assert.*;
import grafo.Assert;
import grafo.Grafo;

import org.junit.Test;

public class SolverTest 
{
	@Test
	public void testVerticesAislados() 
	{
		Grafo g = new Grafo(5);
		Solver solver = new Solver(g);
		assertEquals(1, solver.resolver().size());
	}
	
	@Test
	public void testEjemplo() 
	{
		Grafo g = new Grafo(5);
		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(0, 3);
		g.agregarArista(1, 2);
		g.agregarArista(1, 3);
		g.agregarArista(2, 3);
		g.agregarArista(2, 4);
		Solver solver = new Solver(g);
		
		int[] target = new int[] {0,1,2,3};
		Assert.iguales(target, solver.resolver());
	}
	
	@Test
	public void completoTest()
	{
		Grafo completo = new Grafo(4);
		completo.agregarArista(0, 1);
		completo.agregarArista(0, 2);
		completo.agregarArista(0, 3);
		completo.agregarArista(1, 2);
		completo.agregarArista(1, 3);
		completo.agregarArista(2, 3);
		Solver solver = new Solver(completo);
		
		int[] target = { 0, 1, 2, 3 };

		Assert.iguales(target, solver.resolver());
	}

}
