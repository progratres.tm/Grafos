package maxClique;

import grafo.Grafo;

import java.util.HashSet;
import java.util.Set;


// Representa un individuo dentro de un algoritmo gentico
public class Individuo implements Comparable<Individuo>
{
	// Un individuo se representa con una secuencia de bits
	private boolean[] _bits;
	
	// Grafo asociado
	private Grafo _grafo;
	
	// Generador de nmeros aleatorios
	private static Generador _random;
	
	// Setter del generador de nmeros aleatorios
	public static void setGenerador(Generador random)
	{
		_random = random;
	}
	
	// Individuo aleatorio
	public static Individuo aleatorio(Grafo grafo)
	{
		Individuo ret = new Individuo(grafo);
		
		for(int i=0; i<grafo.vertices(); ++i)
			ret.bit(i, _random.nextBoolean());
		
		return ret;
	}
	
	// Individuo vacio
	private static Individuo vacio(Grafo grafo)
	{
		return new Individuo(grafo);
	}
	
	// El constructor est oculto!
	private Individuo(Grafo grafo)
	{
		_grafo = grafo;
		_bits = new boolean[grafo.vertices()];
	}

	// La mutacin modifica aleatoriamente un bit
	public void mutar()
	{
		int i = _random.nextInt(_bits.length);
		_bits[i] = !_bits[i];
	}
	
	// Recombinacin
	public Individuo[] recombinar(Individuo that)
	{
		// Punto de combinacin
		int k = _bits.length / 2;
		
		Individuo hijo1 = Individuo.vacio(_grafo);
		Individuo hijo2 = Individuo.vacio(_grafo);
		
		for(int i=0; i<k; ++i)
		{
			hijo1.bit(i, this.bit(i));
			hijo2.bit(i, that.bit(i));
		}
		
		for(int i=k; i<_bits.length; ++i)
		{
			hijo1.bit(i, that.bit(i));
			hijo2.bit(i, this.bit(i));
		}
		
		return new Individuo[] { hijo1, hijo2 };
	}
	
	// Funcin de fitness
	public int fitness()
	{
		return _grafo.esClique(vertices()) ? tamao() : 0; 
	}
	
	// Auxiliar: Retorna los vertices como un set
	public Set<Integer> vertices()
	{
		Set<Integer> ret = new HashSet<Integer>();
		for(int i=0; i<_bits.length; ++i) if( bit(i) )
			ret.add(i);
		
		return ret;
	}
	
	// Cantidad de vrtices en el individuo
	public int tamao()
	{
		int ret = 0;
		for(int i=0; i<_bits.length; ++i) if( bit(i) )
			++ret;
		
		return ret;
	}
	
	// Getter de los bits, para los tests
	boolean bit(int i)
	{
		return _bits[i];
	}
	
	// Setter de los bits
	private void bit(int i, boolean valor)
	{
		_bits[i] = valor;
	}


	@Override
	public int compareTo(Individuo otro)
	{
		return this.fitness() - otro.fitness();
	}
}










