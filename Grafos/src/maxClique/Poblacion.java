package maxClique;

import grafo.Grafo;
import java.util.ArrayList;
import java.util.Collections;


// Representa una poblacin en el algoritmo gentico
public class Poblacion
{
	// Conjunto de individuos
	private ArrayList<Individuo> _individuos;

	// Instancia a resolver
	private Grafo _grafo;
	
	// Parmetros de la poblacin
	private int _tamao = 100;
	private int _mutadosPorIteracion = 10;
	private int _recombinadosPorIteracion = 20;
	private int _eliminadosPorIteracion = 60;
	private int _iteracion;
	
	// Generador de nmeros aleatorios
	private static Generador _generador;
	
	// Setter del generador
	public static void setGenerador(Generador generador)
	{
		_generador = generador;
	}
	
	// Constructor
	public Poblacion(Grafo grafo)
	{
		_grafo = grafo;
		_iteracion = 1;
	}
	
	public Individuo simular()
	{
		inicializar();
		while( satisfactoria() == false )
		{
			mutarAlgunos();
			recombinarAlgunos();
			eliminarPeores();
			agregarNuevos();
			
			_iteracion++;
			mostrarEstadisticas();
		}
		
		return Collections.max(_individuos);		
	}

	private void inicializar()
	{
		_individuos = new ArrayList<Individuo>(_tamao);
		for(int i=0; i<_tamao; ++i)
			_individuos.add(Individuo.aleatorio(_grafo));
	}

	private boolean satisfactoria()
	{
		// TODO: Implementar!
		return false;
	}

	private void mutarAlgunos()
	{
		for(int i=0; i<_mutadosPorIteracion; ++i)
			aleatorio().mutar();
	}

	private void recombinarAlgunos()
	{
		for(int i=0; i<_recombinadosPorIteracion; ++i)
		{
			for(Individuo hijo: aleatorio().recombinar(aleatorio()))
				_individuos.add(hijo);
		}
	}
	
	private void eliminarPeores()
	{
		Collections.sort(_individuos);
		Collections.reverse(_individuos);
		
		for(int i=0; i<_eliminadosPorIteracion; ++i)
			_individuos.remove( _individuos.size()-1 );
	}

	private void agregarNuevos()
	{
		while( _individuos.size() < _tamao )
			_individuos.add(Individuo.aleatorio(_grafo));
	}

	private void mostrarEstadisticas()
	{
		System.out.print("It.: " + _iteracion);
		System.out.print(" - Peor: " + Collections.min(_individuos).fitness());
		System.out.print(" - Prom: " + fitnessPromedio());
		System.out.println(" - Mejor: " + Collections.max(_individuos).fitness());
	}

	private double fitnessPromedio()
	{
		double suma = 0;
		for(Individuo individuo: _individuos)
			suma += individuo.fitness();
		
		return suma / _individuos.size();
	}

	private Individuo aleatorio()
	{
		int j = _generador.nextInt(_tamao);
		return _individuos.get(j);	
	}
	
	public int iteracion()
	{
		return _iteracion;	
	}
}
