package maxClique;

import java.util.Random;

import grafo.Grafo;

public class SolverStress
{
	public static void main(String[] args)
	{
		for(int n=5; n<=400; ++n)
		{
			Grafo grafo = aleatorio(n);
			
			Solver solver = new Solver(grafo);
			solver.resolver();
			
			System.out.print("n = " + n + ", tiempo = " + Solver.darTiempo());
			System.out.println(", hojas: " + solver.hojas());
		}
	}
	
	static Grafo aleatorio(int n)
	{
		Random random = new Random(0);
		Grafo grafo = new Grafo(n);
		
		for(int i=0; i<n; ++i)
		for(int j=i+1; j<n; ++j) if( random.nextFloat() < 0.6 )
			grafo.agregarArista(i, j);
		
		return grafo;
	}
}

