package maxClique;

import java.util.Random;

public class GeneradorAleatorio implements Generador 
{
	private Random random = new Random(); 
	@Override
	public boolean nextBoolean() 
	{
		return random.nextBoolean();
	}

	@Override
	public int nextInt(int number) 
	{
		return random.nextInt(number);
	}

}
