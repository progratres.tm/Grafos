package maxClique;

import grafo.Grafo;



public class PoblacionTest
{
	public static void main(String[] args)
	{
		Grafo grafo = SolverStress.aleatorio(20);
		
		Individuo.setGenerador(new GeneradorAleatorio());
		Poblacion.setGenerador(new GeneradorAleatorio());
		
		Poblacion poblacion = new Poblacion(grafo);
		poblacion.simular();
	}
}
